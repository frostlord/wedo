<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $phone;
    public $name;
    public $surname;
    public $patronymic;
    public $platform;
    public $iin;
    public $push_id;
    public $platform_details;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'name', 'surname', 'patronymic', 'platform'],'required'],
            [['platform','push_id', 'platform_details'], 'string'],
            [['phone', 'name', 'surname', 'patronymic'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 11],
            [['iin'], 'string', 'max' => 15],
            ['phone', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone'	=> 'Номер телефона пользователя в формате 7XXXYYYYYYY',
            'name' => 'Имя пользователя',
            'surname' => 'Фамилия пользователя',
            'patronymic' => 'Отчество пользователя',
            'platform' => 'Строка, идентифицирующая платформу. Возможные значения: ios, android',
            'iin' => 'ИИН пользователя',
            'push_id' => 'Идентификатор устройства, который используются для отправки уведомлений через APNS, если он был получен',
            'platform_details' => 'Описание устройства и операционной системы'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->phone = $this->phone;
            $user->firstname = $this->name;
            $user->surname = $this->surname;
            $user->patronymic = $this->patronymic;
            $user->platform = $this->platform;
            $user->iin = $this->iin;
            $user->push_id = $this->push_id;
            $user->platform_details = $this->platform_details;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                $curl = new Curl();
                $curl->init();

                $curl->post("http://smsc.ru/sys/send.php", array(
                    "login" => Yii::$app->params['smsLogin'],
                    "psw" => Yii::$app->params['smsPass'],
                    "phones"=> $user->phone,
                    "mes"=> $user->auth_key,
                ));
            }
        }
        return null;
    }
}
