<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'admin@example.kz',
    'user.passwordResetTokenExpire' => 3600,
];
