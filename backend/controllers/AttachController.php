<?php

namespace backend\controllers;

use Yii;
use common\models\Attachments;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use common\models\UploadForm;

class AttachController extends \yii\web\Controller
{
    public $enableCsrfValidation = true;

    public function behaviors()
    {
        $this->enableCsrfValidation = false;
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'upload'=>['post'],
                ],
            ],
        ];
    }

    public function actionUpload()
    {
        $model = new UploadForm();
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file && $model->validate()) {
                $file = $model->file;
                $ir = $_GET['ir'];
                $file_name = $ir . time() . '.' . $model->file->extension;
                $filepath = Yii::getAlias('@backend/web/upload/') . $file_name;
                if (move_uploaded_file($file->tempName, $filepath)) {

                    $photo = new Attachments();
                    if ($ir == 'recipient') {
                        $photo->recipient_id = $_GET['id'];
                    }
                    if ($ir == 'auction') {
                        $photo->auction_id = $_GET['id'];
                    }
//                    $photo->height =
//                    $photo->width =
                    $photo->file_size = $file->size;
                    $photo->content_type = 'image';
                    $photo->subdir = 'upload/';
                    $photo->file_name = $file_name;
                    $photo->original_name = $file->name;
//                    $photo->description => '��������',
                    $photo->save();
                }
        if ($ir == 'recipient') {
            return $this->redirect(['/recipient/view', 'id' => $_GET['id']]);
        }
        if ($ir == 'auction') {
            return $this->redirect(['/auction/view', 'id' => $_GET['id']]);
        }
            }
        }
        return $this->goBack();
    }
}
