<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\HelpRecipient;

/* @var $this yii\web\View */
/* @var $model common\models\HelpDonation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="help-donation-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form'],
        'fieldConfig' => [
            'template' => '{input}{label}{hint}{error}',
            'options' => ['class' => 'form-group floating-label'],
            'labelOptions' => [],
            'inputOptions' => ['class' => 'form-control'],

        ]]); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'recipient_id')->dropDownList(HelpRecipient::get_recipient(), ['prompt' => '']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'amount', ['options' => ['class' => 'form-group']])->textInput(['maxlength' => true, 'data-inputmask' => "'alias': 'integer'"]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model,'p_date', ['options' => ['class' => 'form-group']])->textInput(['maxlength' => true, 'placeholder'=>'dd.mm.yyyy','data-inputmask' => "'alias': 'dd.mm.yyyy'"]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'payment_system')->dropDownList(['Administration' => 'Администрация',], ['prompt' => '']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

            <div class="forrm-group">
                <?= Html::submitButton($model->isNewRecord ? 'СОЗДАТЬ' : 'СОХРАНИТЬ', ['class' => 'btn ink-reaction ' . ($model->isNewRecord ? 'btn-success' : 'btn-default-bright')]) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

