<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HelpDonationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список пожертвований';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        <?= Html::a('Добавить пожертвование', ['create'], ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

//            'filterModel' => null,
            'layout' => '{items}{summary}{pager}',
            'tableOptions' => ['class' => 'table table-hover'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                [
                    'attribute' => 'recipient_id',
                    'filter' => \common\models\HelpRecipient::get_recipient(),
                    'value' => 'recipient.name',
                ],
                'amount',
                'transfer_date:date',
                'payment_system',
                'comment',
                ['class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '150'],
//                    'template' => ''.($model->payment_system == Administration ? ' {update} ' : '')
                    'template' => '{update} {view} {delete}',
                    'buttons' => [

                        'update' => function ($url, $model) {
                            return $model->payment_system == 'Administration' ? Html::a(
                                '<i class="md-mode-edit"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Редактировать'
                                ]) : '';
                        },
                        'view' => function ($url, $model) {
                            return Html::a(
                                '<i class="md-visibility"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Просмотреть'
                                ]);
                        },
                        'delete' => function ($url, $model) {
                            return  $model->payment_system == 'Administration' ? Html::a(
                                '<i class="md-delete"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Удалить запись',
                                    'data' => [
                                        'confirm' => 'Вы действительно хотите удалить запись?',
                                        'method' => 'post',
                                    ]
                                ]) : '';
                        },
                    ],
                ],
            ],
        ]); ?>

    </div>
</div>
