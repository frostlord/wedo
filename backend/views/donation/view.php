<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\HelpDonation */

$this->title = 'Детальная информация по пожертвованию';
$this->params['breadcrumbs'][] = ['label' => 'Список пожертвований', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="-view">

    <h1>Html::encode($this->title) ?></h1>
-->
<?php if ($model->payment_system == 'Administration') {?>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<?php } ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'recipient.name',
            'amount',
            'p_date',
            'payment_system',
            'comment:ntext',
        ],
    ]) ?>
<!--
</div>
-->