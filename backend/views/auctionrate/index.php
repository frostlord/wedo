<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AuctionRateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список ставок';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="row">
    <div class="col-lg-12">
    <?php /*// echo $this->render('_search', ['model' => $searchModel]); */ ?>
        <? /*= Html::a('Create Auction Rate', ['create'], ['class' => 'btn ink-reaction btn-raised btn-primary']) */ ?>
    </div>
</div>-->
<div class="row">
    <div class="col-lg-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
//            'filterModel' => null,
            'layout' => '{items}{summary}{pager}',
            'tableOptions' => ['class' => 'table table-hover'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'created_by',
                    'filter' => User::get_users(),
                    'format' => 'raw',
                    'value' =>
                        function ($model) {
                            return Html::a($model->createdBy->fio, ['user/view', 'id' => $model->createdBy->id],
                                ['class' => 'btn ink-reaction btn-flat btn-xs btn-primary']);
                        }
                ],
                'createdBy.phone',
                [
                    'attribute' => 'auction_lot_id',
                    'filter' => User::get_users(),
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model->auctionLot->name, ['auction/view', 'id' => $model->auctionLot->id],
                            ['class' => 'btn ink-reaction btn-flat btn-xs btn-primary']);
                    }
                ],
                'rate_amount',
                'rate_time:datetime',
                'created_at:datetime',
                /*
                                ['class' => 'yii\grid\ActionColumn',
                                    'headerOptions' => ['width' => '150'],
                                    'template' => '{update} {view} {delete}',
                                    'buttons' => [
                                        'update' => function ($url, $model) {
                                            return Html::a(
                                                '<i class="md-mode-edit"></i>',
                                                $url,
                                                [
                                                    'type' => 'button',
                                                    'class' => 'btn btn-icon-toggle',
                                                    'data-toggle' => 'tooltip',
                                                    'data-placement' => 'top',
                                                    'data-original-title' => 'Редактировать'
                                                ]);
                                        },
                                        'view' => function ($url, $model) {
                                            return Html::a(
                                                '<i class="md-visibility"></i>',
                                                $url,
                                                [
                                                    'type' => 'button',
                                                    'class' => 'btn btn-icon-toggle',
                                                    'data-toggle' => 'tooltip',
                                                    'data-placement' => 'top',
                                                    'data-original-title' => 'Просмотреть'
                                                ]);
                                        },
                                        'delete' => function ($url, $model) {
                                            return Html::a(
                                                '<i class="md-delete"></i>',
                                                $url,
                                                [
                                                    'type' => 'button',
                                                    'class' => 'btn btn-icon-toggle',
                                                    'data-toggle' => 'tooltip',
                                                    'data-placement' => 'top',
                                                    'data-original-title' => 'Удалить запись',
                                                    'data' => [
                                                        'confirm' => 'Вы действительно хотите удалить запись?',
                                                        'method' => 'post',
                                                    ]
                                                ]);
                                        },
                                    ],
                                ],*/
            ],
        ]); ?>

    </div>
</div>
