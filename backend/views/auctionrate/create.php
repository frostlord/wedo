<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AuctionRate */

$this->title = 'Добавление ' .'Auction Rate';
$this->params['breadcrumbs'][] = ['label' => 'Список ставок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="--><!---create">-->

<!--    <h1>--><!--Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<!--</div>-->
