<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AuctionRate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auction-rate-form">

    <?php $form = ActiveForm::begin([
        'options'=>['class'=>'form'],
        'fieldConfig' => [
            'template' => '{input}{label}{hint}{error}',
            'options' => ['class' => 'form-group floating-label'],
            'labelOptions' => [],
            'inputOptions' => ['class' => 'form-control'],

        ]]); ?>
    <?= $form->field($model, 'auction_lot_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rate_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rate_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
