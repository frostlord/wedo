<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title>WeDoCharity</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <?= Html::csrfMetaTags() ?>
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href="http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900" rel="stylesheet"
          type="text/css">
    <link type="text/css" rel="stylesheet" href="../../assets/css/theme-default/bootstrap.css?1422792965">
    <link type="text/css" rel="stylesheet" href="../../assets/css/theme-default/materialadmin.css?1425466319">
    <link type="text/css" rel="stylesheet" href="../../assets/css/theme-default/font-awesome.min.css?1422529194">
    <link type="text/css" rel="stylesheet"
          href="../../assets/css/theme-default/material-design-iconic-font.min.css?1421434286">

    <link type="text/css" rel="stylesheet"
          href="../../assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858"/>
    <!-- END STYLESHEETS -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="../../assets/js/libs/utils/html5shiv.js?1403934957"></script>
    <script type="text/javascript" src="../../assets/js/libs/utils/respond.min.js?1403934956"></script>
    <![endif]-->
    <style type="text/css"></style>
</head>
<body class="menubar-hoverable header-fixed menubar-pin ">
<?php $this->beginBody() ?>

<!-- BEGIN HEADER-->
<header id="header">
    <div class="headerbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li class="header-nav-brand">
                    <div class="brand-holder">
                        <a href="#">
                            <span class="text-lg text-bold text-primary">WeDoCharity</span>
                        </a>
                    </div>
                </li>
                <li>
                    <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="headerbar-right">
            <ul class="header-nav header-nav-profile">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
								<span class="profile-info">
									<?= Yii::$app->user->identity->email ?>
                                    <small>Administrator</small>
								</span>
                    </a>
                    <ul class="dropdown-menu animation-dock">
                        <li>
                            <?= Html::a('<i class="fa fa-fw fa-power-off text-danger"></i> Logout', ['/site/logout'], [
                                'data' => [
                                    'method' => 'post',
                                ]
                            ]) ?>
                        </li>
                    </ul>
                    <!--end .dropdown-menu -->
                </li>
                <!--end .dropdown -->
            </ul>
            <!--end .header-nav-profile -->

        </div>
        <!--end #header-navbar-collapse -->
    </div>
</header>
<!-- END HEADER-->

<!-- BEGIN BASE-->
<div id="base">

    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas">
    </div>
    <!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->

    <!-- BEGIN CONTENT-->
    <div id="content">

        <!-- BEGIN BLANK SECTION -->
        <section class="style-default-bright">
            <div class="section-header">
                <?php /*= Breadcrumbs::widget([
                    'itemTemplate' => "<li>{link}</li>\n", // template for all links

                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                */ ?>
                <h2 class="text-primary"><?= Html::encode($this->title) ?></h2>
            </div>
            <!--end .section-header -->
            <div class="section-body">
                <?= $content ?>
            </div>
            <!--end .section-body -->
        </section>

    </div>
    <!--end #content-->
    <!-- END CONTENT -->

    <!-- BEGIN MENUBAR-->
    <div id="menubar" class="menubar-inverse  animate">
        <div class="menubar-fixed-panel">
            <div>
                <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar"
                   href="javascript:void(0);">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
        <div style="height: 343px;" class="nano has-scrollbar">
            <div style="right: -17px;" tabindex="0" class="nano-content">
                <div style="padding-bottom: 55px;" class="menubar-scroll-panel">

                    <!-- BEGIN MAIN MENU -->
                    <ul id="main-menu" class="gui-controls">

                        <!-- BEGIN DASHBOARD -->
                        <li class="gui-folder expanded">
                            <a>
                                <div class="gui-icon"><i class="md md-home"></i></div>
                                <span class="title">Адресная помощь</span>
                            </a>
                            <ul>
                                <li><?= Html::a('<span class="title">Список получателей</span>', ['recipient/index']) ?></li>
                                <li><?= Html::a('<span class="title">Список пожертвований</span>', ['donation/index']) ?></li>
                            </ul>
                            <!--end /submenu -->
                        </li>
                        <!--end /menu-li -->
                        <!-- END DASHBOARD -->

                        <!-- BEGIN FORMS -->
                        <li class="gui-folder expanded">
                            <a>
                                <div class="gui-icon"><span class="glyphicon glyphicon-list-alt"></span></div>
                                <span class="title">Аукцион</span>
                            </a>
                            <!--start submenu -->
                            <ul>
                                <li><?= Html::a('<span class="title">Список лотов</span>', ['auction/index']) ?></li>
                                <li><?= Html::a('<span class="title">Участники аукциона</span>', ['user/index']) ?></li>
                                <li><?= Html::a('<span class="title">Список ставок</span>', ['auctionrate/index']) ?></li>
                            </ul>
                            <!--end /submenu -->
                        </li>
                        <!--end /menu-li -->
                        <!-- END FORMS -->


                    </ul>
                    <!--end .main-menu -->
                    <!-- END MAIN MENU -->

                    <!--  <div class="menubar-foot-panel">
                          <small class="no-linebreak hidden-folded">
                              <span class="opacity-75">Copyright © 2014</span> <strong>CodeCovers</strong>
                          </small>
                      </div>-->
                </div>
            </div>
            <div class="nano-pane">
                <div style="height: 124px; transform: translate(0px, 0px);" class="nano-slider"></div>
            </div>
        </div>
        <!--end .menubar-scroll-panel-->
    </div>
    <!--end #menubar-->
    <!-- END MENUBAR -->

    <!-- BEGIN OFFCANVAS RIGHT -->
    <div class="offcanvas">
        <!-- BEGIN OFFCANVAS CHAT -->
        <div id="offcanvas-chat" class="offcanvas-pane style-default-light width-12">
            <div class="offcanvas-head style-default-bright">
                <header class="text-primary">Chat with Ann Laurens</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                    <a class="btn btn-icon-toggle btn-default-light pull-right" href="#offcanvas-search"
                       data-toggle="offcanvas" data-backdrop="false">
                        <i class="md md-arrow-back"></i>
                    </a>
                </div>
                <form class="form">
                    <div class="form-group floating-label">
                        <textarea name="sidebarChatMessage" id="sidebarChatMessage" class="form-control autosize"
                                  rows="1"></textarea>
                        <label for="sidebarChatMessage">Leave a message</label>
                    </div>
                </form>
            </div>
            <div class="offcanvas-body">
                <ul class="list-chats">
                    <li>
                        <div class="chat">
                            <div class="chat-avatar"><img class="img-circle"
                                                          src="../../assets/img/avatar1.jpg?1403934956" alt=""></div>
                            <div class="chat-body">
                                Yes, it is indeed very beautiful.
                                <small>10:03 pm</small>
                            </div>
                        </div>
                        <!--end .chat -->
                    </li>
                    <li class="chat-left">
                        <div class="chat">
                            <div class="chat-avatar"><img class="img-circle"
                                                          src="../../assets/img/avatar9.jpg?1404026744" alt=""></div>
                            <div class="chat-body">
                                Did you see the changes?
                                <small>10:02 pm</small>
                            </div>
                        </div>
                        <!--end .chat -->
                    </li>
                    <li>
                        <div class="chat">
                            <div class="chat-avatar"><img class="img-circle"
                                                          src="../../assets/img/avatar1.jpg?1403934956" alt=""></div>
                            <div class="chat-body">
                                I just arrived at work, it was quite busy.
                                <small>06:44pm</small>
                            </div>
                            <div class="chat-body">
                                I will take look in a minute.
                                <small>06:45pm</small>
                            </div>
                        </div>
                        <!--end .chat -->
                    </li>
                    <li class="chat-left">
                        <div class="chat">
                            <div class="chat-avatar"><img class="img-circle"
                                                          src="../../assets/img/avatar9.jpg?1404026744" alt=""></div>
                            <div class="chat-body">
                                The colors are much better now.
                            </div>
                            <div class="chat-body">
                                The colors are brighter than before.
                                I have already sent an example.
                                This will make it look sharper.
                                <small>Mon</small>
                            </div>
                        </div>
                        <!--end .chat -->
                    </li>
                    <li>
                        <div class="chat">
                            <div class="chat-avatar"><img class="img-circle"
                                                          src="../../assets/img/avatar1.jpg?1403934956" alt=""></div>
                            <div class="chat-body">
                                Are the colors of the logo already adapted?
                                <small>Last week</small>
                            </div>
                        </div>
                        <!--end .chat -->
                    </li>
                </ul>
            </div>
            <!--end .offcanvas-body -->
        </div>
        <!--end .offcanvas-pane -->
        <!-- END OFFCANVAS CHAT -->


        <!-- begin edit Entity -->
        <?php
        if (isset($this->blocks['block1']))
            echo $this->blocks['block1']; ?>


        <!-- begin edit Entity -->
    </div>
    <!--end .offcanvas-->
    <!-- END OFFCANVAS RIGHT -->

</div>
<!--end #base-->
<!-- END BASE -->

<!-- BEGIN JAVASCRIPT -->
<script src="../../assets/js/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="../../assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
<!--<script src="../../assets/js/libs/jquery-ui/jquery-ui.min.js"></script>-->
<script src="../../assets/js/libs/bootstrap/bootstrap.min.js"></script>
<script src="../../assets/js/libs/spin.js/spin.min.js"></script>
<script src="../../assets/js/libs/autosize/jquery.autosize.min.js"></script>
<script src="../../assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
<script src="../../assets/js/libs/inputmask/jquery.inputmask.bundle.min.js"></script>
<script src="../../assets/js/core/source/App.js"></script>
<script src="../../assets/js/core/source/AppNavigation.js"></script>
<script src="../../assets/js/core/source/AppOffcanvas.js"></script>
<script src="../../assets/js/core/source/AppCard.js"></script>
<script src="../../assets/js/core/source/AppForm.js"></script>
<script src="../../assets/js/core/source/AppNavSearch.js"></script>
<script src="../../assets/js/core/source/AppVendor.js"></script>
<script src="../../assets/js/core/demo/Demo.js"></script>
<script src="../../assets/js/core/demo/DemoFormComponents.js"></script>
<!-- END JAVASCRIPT -->


<div id="device-breakpoints">
    <div class="device-xs visible-xs" data-breakpoint="xs"></div>
    <div class="device-sm visible-sm" data-breakpoint="sm"></div>
    <div class="device-md visible-md" data-breakpoint="md"></div>
    <div class="device-lg visible-lg" data-breakpoint="lg"></div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
