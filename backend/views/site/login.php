<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin([
    'options'=>['class'=>'form'],
    'fieldConfig' => [
        'template' => '{input}{label}{hint}{error}',
        'options' => ['class' => 'form-group floating-label'],
        'labelOptions' => [],
        'inputOptions' => ['class' => 'form-control'],
    ]]); ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'password')->passwordInput() ?>


   <!-- <div class="form-group required">
        <input type="password" class="form-control" id="password" name="LoginForm[password]">
        <label for="password">Password</label>
        <p class="help-block"><a href="#">Forgotten?</a></p>
    </div>
    <br/>-->
    <div class="row">
    <!--    <div class="col-xs-6 text-left">
            <div class="checkbox checkbox-inline checkbox-styled">
                <label>
                    <input type="checkbox"name="LoginForm[rememberMe]"><span>Запомнить меня</span>
                </label>
            </div>
        </div><!--end .col -->
<!--        <div class="col-xs-6 text-right">-->
            <?= Html::submitButton('Воити', ['class' => 'btn btn-primary btn-raised']) ?>
<!--        </div><!--end .col -->
    </div><!--end .row -->
    <?php ActiveForm::end(); ?>