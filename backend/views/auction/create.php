<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AuctionLot */

$this->title = 'Добавление лота';
$this->params['breadcrumbs'][] = ['label' => 'Список лотов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="--><!---create">-->

<!--    <h1>--><!--Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

<!--</div>-->
