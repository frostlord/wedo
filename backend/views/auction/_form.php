<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AuctionLot */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auction-lot-form">

    <?php $form = ActiveForm::begin([
        'options'=>['class'=>'form'],
        'fieldConfig' => [
            'template' => '{input}{label}{hint}{error}',
            'options' => ['class' => 'form-group floating-label'],
            'labelOptions' => [],
            'inputOptions' => ['class' => 'form-control'],

        ]]); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'p_time_begin', ['options' => ['class' => 'form-group']])->textInput(['maxlength' => true, 'placeholder'=>'dd.mm.yyyy', 'data-inputmask'=>"'alias': 'dd.mm.yyyy'"]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'p_time_end', ['options' => ['class' => 'form-group']])->textInput(['maxlength' => true, 'placeholder'=>'dd.mm.yyyy', 'data-inputmask'=>"'alias': 'dd.mm.yyyy'"]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
            <div class="forrm-group">
                <?= Html::submitButton($model->isNewRecord ? 'СОЗДАТЬ' : 'СОХРАНИТЬ', ['class' => 'btn ink-reaction ' . ($model->isNewRecord ? 'btn-success' : 'btn-default-bright')]) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
