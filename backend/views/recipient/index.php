<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HelpRecipientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список получателей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


        <?= Html::a('Добавить получателя', ['create'], ['class' => 'btn ink-reaction btn-raised btn-primary']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => '{items}{summary}{pager}',
            'tableOptions' => ['class' => 'table table-hover'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'amount',
                'receivedamount',
                'is_active:boolean',
                ['class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '150'],
                    'template' => '{update} {view} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a(
                                '<i class="md-mode-edit"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Редактировать'
                                ]);
                        },
                        'view' => function ($url, $model) {
                            return Html::a(
                                '<i class="md-visibility"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Просмотреть'
                                ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a(
                                '<i class="md-delete"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Удалить запись',
                                    'data' => [
                                        'confirm' => 'Вы действительно хотите удалить запись?',
                                        'method' => 'post',
                                    ]
                                ]);
                        },
                    ],
                ],
            ],
        ]); ?>

    </div>
</div>
