<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HelpRecipient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="help-recipient-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form'],
        'fieldConfig' => [
            'template' => '{input}{label}{hint}{error}',
            'options' => ['class' => 'form-group floating-label'],
            'labelOptions' => [],
            'inputOptions' => ['class' => 'form-control'],

        ]]); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <div class="forrm-group">
        <?= Html::submitButton($model->isNewRecord ? 'СОЗДАТЬ' : 'СОХРАНИТЬ', ['class' => 'btn ink-reaction ' . ($model->isNewRecord ? 'btn-success' : 'btn-default-bright')]) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
