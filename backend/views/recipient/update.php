<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HelpRecipient */

$this->title = 'Редактирование получателя: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список получателей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
