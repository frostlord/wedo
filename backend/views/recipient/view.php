<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model common\models\HelpRecipient */

$this->title = 'Детальная информация по получателю: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Help Recipients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<p>
    <?= Html::a(($model->is_active) ? 'Даективировать' : 'Активировать', ['changeactive', 'id' => $model->id],
        ['class' => 'btn ink-reaction ' . (($model->is_active) ? 'btn-default-light' : 'btn-primary-bright'),
            'data' => [
                'confirm' => 'Вы действительно хотите изменить состояние активности?',
                'method' => 'post',
            ],]) ?>
    <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn ink-reaction btn-primary']) ?>
    <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn ink-reaction btn-danger',
        'data' => [
            'confirm' => 'Вы действительно хотите удалить эту запись?',
            'method' => 'post',
        ],
    ]) ?>
    <button data-toggle="modal" data-target="#firstModal" class="btn ink-reaction btn-accent-light">Загрузить фото</button>
</p>

<div class="card tabs-left style-default-light">
    <ul class="card-head nav nav-tabs" data-toggle="tabs">
        <li class="active"><a href="#first5">ИНФО</a></li>
        <li class=""><a href="#second5">ФОТО</a></li>
    </ul>
    <div class="card-body tab-content style-default-bright">
        <div class="tab-pane active" id="first5">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'description:ntext',
                    'amount',
                    'receivedamount',
                    'is_active:boolean'
                ],
            ]) ?>
        </div>
        <div class="tab-pane" id="second5">
            <div class="list-results list-results-underlined">
                <?php foreach ($model->attachments as $v): ?>
                    <div class="col-xs-12">
                        <img class="pull-left width-3" src="<?= $v->path ?>" >
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!--end .card-body -->
</div>
<!--end .card -->


<div class="modal fade" id="firstModal" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="w0" action="/attach/upload?ir=recipient&id=<?= $model->id ?>" method="post"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="simpleModalLabel">ЗАГРУЗКА ФОТО</h4>
                </div>
                <div class="modal-body">
                    <label class="control-label" for="uploadform-file">ФАЙЛ</label>
                    <input type="hidden" name="UploadForm[file]" value=""><input type="file"
                                                                                 id="uploadform-file"
                                                                                 name="UploadForm[file]">
                    <input type="hidden" name="UploadForm[id]" value="<?= $model->id ?>">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">НЕ ЗАГРУЖАТЬ</button>
                    <input type="submit" value="ЗАГРУЗИТЬ" class="btn btn-primary">
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
