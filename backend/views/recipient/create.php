<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\HelpRecipient */

$this->title = 'Добавление получателя';
$this->params['breadcrumbs'][] = ['label' => 'Список получателей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
