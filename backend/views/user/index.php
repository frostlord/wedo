<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Участники аукциона';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="row">
    <div class="col-lg-12">
        <?php /*// echo $this->render('_search', ['model' => $searchModel]); */?>


        <?/*= Html::a('Create User', ['create'], ['class' => 'btn ink-reaction btn-raised btn-primary']) */?>
    </div>
</div>-->
<div class="row">
    <div class="col-lg-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,

//            'filterModel' => null,
            'layout' => '{items}{summary}{pager}',
            'tableOptions' => ['class' => 'table table-hover'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'surname',
                'firstname',
                'patronymic',
                'phone',
                'email:email',
                'iin',
                [
                    'attribute' => 'status',
                    'filter' => User::get_statuses(),
                    'value' => 'statusname',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '150'],
                    'template' => '{update} {view} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a(
                                '<i class="md-mode-edit"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Редактировать'
                                ]);
                        },
                        'view' => function ($url, $model) {
                            return Html::a(
                                '<i class="md-visibility"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Просмотреть'
                                ]);
                        },
                        'delete' => function ($url, $model) {
                            return ($model->status != User::STATUS_DELETED && $model->role != User::ROLE_ADMIN) ? Html::a(
                                '<i class="md md-block text-danger"></i>',
                                $url,
                                [
                                    'type' => 'button',
                                    'class' => 'btn btn-icon-toggle',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-original-title' => 'Заблокировать',
                                    'data' => [
                                        'confirm' => 'Вы действительно хотите Заблокировать пользователя?',
                                        'method' => 'post',
                                    ]
                                ]) : '';
                        },
                    ],
                ],
            ],
        ]); ?>

    </div>
</div>
