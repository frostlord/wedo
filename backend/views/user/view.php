<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Просмотр данных участника: '.$model->firstname;
$this->params['breadcrumbs'][] = ['label' => 'Участники аукциона', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<div class="-view">

    <h1>Html::encode($this->title) ?></h1>
-->
    <p>
        <?= ($model->role == User::ROLE_ADMIN) ? '' :
            (Html::a(($model->status != User::STATUS_DELETED ? 'ЗАБЛОКИРОВАТЬ' : 'РАЗБЛОКИРОВАТЬ'), ['changeactive', 'id' => $model->id],
            ['class' => 'btn ink-reaction ' . (($model->status != User::STATUS_DELETED ) ? 'btn-default-light' : 'btn-primary-bright'),
                'data' => [
                    'confirm' => 'Вы действительно хотите изменить состояние активности?',
                    'method' => 'post',
                ],])) ?>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить эту запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'firstname',
            'surname',
            'patronymic',
            'phone',
            'email:email',
            'iin',
            'statusname',
        ],
    ]) ?>
<!--
</div>
-->