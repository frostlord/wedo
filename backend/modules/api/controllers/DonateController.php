<?php

namespace app\modules\api\controllers;

use common\models\HelpRecipient;
use yii\data\Pagination;
use yii\helpers\Json;

class DonateController extends \yii\web\Controller
{
    public function actionDetails($donate_id, $thumb_preset = '300c', $images_preset = '640w')
    {
        $model = HelpRecipient::findOne($donate_id);

        return  [
            'success'=>true,
            'code' => 200,
            'data' => $model
        ];
    }

    public function actionList($thumb_preset = '300c', $images_preset = '640w', $page = 1, $page_size = 10)
    {
        $query = HelpRecipient::find();

        $pagination = new Pagination([
            'pageParam' => $page,
            'defaultPageSize' => $page_size,
            'totalCount' => $query->count(),
        ]);

        $data = $query->
        offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return [
            'success'=>true,
            'code' => 200,
            'data' => $data,
            'pagination' => [
                'page' => $page,
                'page_size' => $page_size,
                'total_item_count' => $pagination->totalCount,//	����� ���������� ���������  � ������
                'total_page_count' => $pagination->pageCount,//	����� ���������� �������
            ]
        ];
    }

}
