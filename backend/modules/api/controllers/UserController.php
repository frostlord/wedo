<?php

namespace app\modules\api\controllers;

use frontend\models\SignupForm;
use yii;
use common\models\User;
use yii\helpers\Json;
use yii\web\Controller;


class UserController extends Controller
{
    public function actionGet()
    {
        $model = User::findOne(Yii::$app->user->identity->id);
        return $model;
    }

    public function actionSignin($phone, $platform, $sms_code = null, $platform_details = null)
    {
        return 'phone:' . $phone . '&platform:' . $platform . '&$sms_code:' . $sms_code . '&$platform_details' . $platform_details;
    }

    public function actionSignup($phone, $name, $surname, $patronymic, $platform, $iin = null, $push_id = null, $platform_details = null)
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->signup();
        }
        return;
    }

    public function actionUpdate($phone = null, $name = null, $surname = null, $patronymic = null, $platform = null, $iin = null, $push_id = null, $platform_details = null, $settings_push_allowed = null)
    {
        if (Yii::$app->request->isPost) {
            $model = User::findOne(Yii::$app->user->identity->id);
            $model->firstname = $name;
            $model->surname = $surname;
            $model->patronymic = $patronymic;
            $model->phone = $phone;
            $model->iin = $iin;
            $model->platform = $platform;
            $model->push_id = $push_id;
            $model->platform_details = $platform_details;
            $model->settings_push_allowed = $settings_push_allowed;
            $model->save();
        }
        return;
    }

}
