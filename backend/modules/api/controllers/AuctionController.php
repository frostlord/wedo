<?php

namespace app\modules\api\controllers;

use yii;
use common\models\AuctionLot;
use common\models\AuctionRate;
use yii\data\Pagination;
use yii\helpers\Json;

class AuctionController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'ruleConfig'=>['class' => AccessRule::className(),],
//                'only' => ['index', 'create', 'update', 'delete', 'view','changeactive'],
//                'rules' => [
//                    [
//                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'changeactive'],
//                        'allow' => true,
//                        'roles' => [User::ROLE_ADMIN]
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                ],
//            ],
        ];
    }
    public function actionBet($lot_id, $bet)
    {
        $model = AuctionLot::findOne($lot_id);
        if ($model->recentRate < $bet) {
            $rate = new AuctionRate();
            $rate->auction_lot_id = $lot_id;
            $rate->rate_amount = $bet;
            $rate->rate_time = time();
            $rate->save();
        }

        return
        [
            'success'=>true,
            'code' => 200,
            'data' => $model->recentRate
        ];
    }

    public function actionDetails($lot_id, $thumb_preset = '300c', $images_preset = '640w')
    {
        $model = AuctionLot::findOne($lot_id);
        return  [
            'success'=>true,
            'code' => 200,
            'data' => $model
        ];
    }

    public function actionList($involved_only = null, $lead_only = null, $won_only = null, $thumb_preset = '300c', $images_preset = '640w', $page = 1, $page_size = 10)
    {

        $query = AuctionLot::find();
        if ($involved_only != null) {
            $involved = AuctionRate::find()->select('auction_lot_id')->where('created_by = :userid', [':userid' => Yii::$app->user->identity->id])->all();
            $query->where([
                'id' => [$involved],
            ]);
        } else
            if ($lead_only != null) {
                $query->where('recentRateowne = :userid', [':userid' => Yii::$app->user->identity->id]);
            } else
                if ($won_only != null) {
                    $query->where('recentRateowne = :userid', [':userid' => Yii::$app->user->identity->id]);
                    $query->andWhere('time_end<:date', [':date' => date_create(time())]);
                }

        $pagination = new Pagination([
            'pageParam' => $page,
            'defaultPageSize' => $page_size,
            'totalCount' => $query->count(),
        ]);

        $data = $query->
        offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return [
            'success'=>true,
            'code' => 200,
            'data' => $data,
            'pagination' => [
                'page' => $page,
                'page_size' => $page_size,
                'total_item_count' => $pagination->totalCount,//	����� ���������� ���������  � ������
                'total_page_count' => $pagination->pageCount,//	����� ���������� �������
            ]
        ];
    }


}
