<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "attachments".
 *
 * @property string $id
 * @property string $recipient_id
 * @property string $auction_id
 * @property integer $height
 * @property integer $width
 * @property string $file_size
 * @property string $content_type
 * @property string $subdir
 * @property string $file_name
 * @property string $original_name
 * @property string $description
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property HelpRecipient $recipient
 * @property User $createdBy
 * @property User $updatedBy
 * @property AuctionLot $auction
 */
class Attachments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attachments';
    }

    public function fields()
    {
        return [
            'url'=>'path'
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_size', 'file_name'], 'required'],
            [['recipient_id', 'auction_id', 'height', 'width', 'file_size', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['file_name'], 'string'],
            [['content_type', 'subdir', 'original_name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор записи',
            'recipient_id' => 'Получатель помощи',
            'auction_id' => 'Изображение лота',
            'height' => 'Высота картинки',
            'width' => 'Ширина картинки',
            'file_size' => 'Размер файла',
            'content_type' => 'Тип контента',
            'subdir' => 'Директория',
            'file_name' => 'Имя файла',
            'original_name' => 'оригинальное имя файла',
            'description' => 'Описание',
            'created_at' => 'Время добавления записи',
            'created_by' => 'Кем добавлена запись',
            'updated_at' => 'Время последнего обновления записи',
            'updated_by' => 'Кто последний обновил запись',
        ];
    }

    public function getPath()
    {
        return Yii::$app->request->hostInfo.'/'.$this->subdir.$this->file_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(HelpRecipient::className(), ['id' => 'recipient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuction()
    {
        return $this->hasOne(AuctionLot::className(), ['id' => 'auction_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }
}
