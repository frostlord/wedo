<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\HelpDonation;

/**
 * HelpDonationSearch represents the model behind the search form about `common\models\HelpDonation`.
 */
class HelpDonationSearch extends HelpDonation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'recipient_id', 'transfer_date'], 'integer'],
            [['amount'], 'number'],
            [['payment_system'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HelpDonation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'recipient_id' => $this->recipient_id,
            'amount' => $this->amount,
            'transfer_date' => $this->transfer_date,
        ]);

        $query->andFilterWhere(['like', 'payment_system', $this->payment_system]);

        return $dataProvider;
    }
}
