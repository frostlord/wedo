<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "auction_rate".
 *
 * @property string $id
 * @property string $auction_lot_id
 * @property string $rate_amount
 * @property string $rate_time
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property AuctionLot $auctionLot
 * @property User $createdBy
 * @property User $updatedBy
 */
class AuctionRate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auction_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auction_lot_id', 'rate_time', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['rate_amount', 'rate_time'], 'required'],
            [['rate_amount'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор записи',
            'auction_lot_id' => 'Лот',
            'rate_amount' => 'Сумма ставки',
            'rate_time' => 'Время ставки',
            'created_at' => 'Дата и время ставки',
            'created_by' => 'ФИО пользователя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuctionLot()
    {
        return $this->hasOne(AuctionLot::className(), ['id' => 'auction_lot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }
}
