<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AuctionRate;

/**
 * AuctionRateSearch represents the model behind the search form about `common\models\AuctionRate`.
 */
class AuctionRateSearch extends AuctionRate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'auction_lot_id', 'rate_time', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['rate_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AuctionRate::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'auction_lot_id' => $this->auction_lot_id,
            'rate_amount' => $this->rate_amount,
            'rate_time' => $this->rate_time,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
