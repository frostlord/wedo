<?php

namespace common\models;

use Faker\Provider\DateTime;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "auction_lot".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $time_begin
 * @property string $time_end
 * @property integer $is_active
 * @property string $created_at
 * @property string $created_by
 * @property string $updatet_at
 * @property string $update_by
 *
 * @property Attachments[] $attachments
 * @property User $createdBy
 * @property User $updateBy
 * @property AuctionRate[] $auctionRates
 */
class AuctionLot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auction_lot';
    }

    public function fields()
    {
        return [
            'id' => 'id',
            'title' => 'name',
            'description' => 'description',
//            'thumb' => null,// 'empty url',
            'last_bet' => 'recentRate',
            'is_last_bet_owner' => function(){return ($this->recentRateowner && $this->recentRateowner == Yii::$app->user->identity->id);},
            'time_end' => function(){
                $date = date_create();
                date_timestamp_set($date, $this->time_end);
                return $date->format('Y-m-d\TH:i:sO');
            },
            'images' => function(){return (count($this->attachments)>0)? ArrayHelper::getColumn($this->attachments, 'path') : null;},
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'time_begin', 'time_end', 'p_time_begin', 'p_time_end'], 'required'],
            [['description'], 'string'],
            [['time_begin', 'time_end', 'is_active', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['p_time_begin', 'p_time_end'], 'date']
        ];
    }

    public function getP_time_end() {
        return $this->time_end ? Yii::$app->formatter->asDate($this->time_end) : '';
    }

    public function setP_time_end($date) {
        $this->time_end = $date ? strtotime($date) : null;
    }

    public function getP_time_begin() {
        return $this->time_begin ? Yii::$app->formatter->asDate($this->time_begin) : '';
    }

    public function setP_time_begin($date) {
        $this->time_begin = $date ? strtotime($date) : null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор записи',
            'name' => 'Наименование',
            'description' => 'Описание',
            'time_begin' => 'Время начала',
            'time_end' => 'Время конца',
            'p_time_begin' => 'Время начала',
            'p_time_end' => 'Время конца',
            'is_active' => 'Активность',
            'recentRate' => 'Сумма последней ставки'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachments::className(), ['auction_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'update_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuctionRates()
    {
        return $this->hasMany(AuctionRate::className(), ['auction_lot_id' => 'id']);
    }

    public function getRecentRate()
    {
        $model = AuctionRate::find()->where('auction_lot_id = :id', [':id' => $this->id])
            ->orderBy(['rate_amount' => SORT_DESC])->one();
        return ($model== null) ? 0 : $model->rate_amount;
    }

    public function getRecentRateowner()
    {
        $model = AuctionRate::find()->where('auction_lot_id = :id', [':id' => $this->id])
            ->orderBy(['rate_amount' => SORT_DESC])->one();
        return ($model== null) ? 0 : $model->created_by;
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }
}
