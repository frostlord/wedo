<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
/**
 * User model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const ROLE_USER = 0;
    const ROLE_ADMIN = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function fields()
    {
        return [
            'phone' => 'phone',
            'name' => 'firstname',
            'surname' => 'surname',
            'patronymic' => 'patronymic',
            'platform' => 'platform',
            'iin' => 'iin',
            'push_id' => 'push_id',
            'platform_details' => 'platform_details',
            'settings_push_allowed' => settings_push_allowed,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['role', 'in', 'range'=>[self::ROLE_USER, self::ROLE_ADMIN]],
            [['firstname','surname','patronymic','phone','email', 'iin', 'push_id', 'platform_details'], 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['settings_push_allowed', 'default', 'value'=>0],
            ['settings_push_allowed', 'in', 'range'=>[0,1]],
            ['platform', 'in', 'range' => ['ios', 'android']],

        ];
    }




    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'firstname' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'phone' => 'Телефон',
            'email' => 'Email',
            'iin' => 'ИИН',
            'status' => 'Статус',
            'statusname' => 'Статус',
            'role' => 'Роль',
            'rolename'=> 'Роль',
            'platform' => 'Строка, идентифицирующая платформу',
            'push_id' => 'Идентификатор устройства, который используются для отправки уведомлений через APNS',
            'platform_details' => 'Описание устройства и операционной системы',
            'settings_push_allowed' => 'Флаг для включения/выключения отправки push-уведомлений с сервера',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomKey(5);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord)
        {
            $this->role=self::ROLE_USER;
            $this->status=self::STATUS_DELETED;
        }
        return parent::beforeSave($insert);
    }

    public function getRolename()
    {
        $rolename = '';

        switch($this->role) {
            case self::ROLE_USER: $rolename='Пользователь'; break;
            case self::ROLE_ADMIN: $rolename='Администратор'; break;
        }
        return $rolename;
    }

    public function getStatusname()
    {
        $statusname = '';
        switch($this->status) {
            case self::STATUS_DELETED: $statusname='Удален/Заблокирован'; break;
            case self::STATUS_ACTIVE: $statusname='Активен';; break;
        }
        return $statusname;
    }

    public static function get_bool(){
        $roles = [
            ['id'=>0, 'name'=>'Нет'],
            ['id'=>1, 'name'=>'Да']];
        $roles = ArrayHelper::map($roles, 'id', 'name');
        return $roles;
    }

    public static function get_statuses(){
        $statuses = [
            ['id'=>self::STATUS_DELETED, 'name'=>'Удален/Заблокирован'],
            ['id'=>self::STATUS_ACTIVE, 'name'=>'Активен'],
            ];
        $statuses = ArrayHelper::map($statuses, 'id', 'name');
        return $statuses;
    }
    public function getFio()
    {
        return $this->surname.' '.$this->firstname.' '.$this->patronymic;
    }
    public static function get_users()
    {
        return ArrayHelper::map(User::find()->all(),'id', 'fio');
    }
}
