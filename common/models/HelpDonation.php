<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "help_donation".
 *
 * @property string $id
 * @property string $recipient_id
 * @property string $amount
 * @property string $transfer_date
 * @property string $payment_system
 * @property string $comment
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property HelpRecipient $recipient
 * @property User $createdBy
 * @property User $updatedBy
 */
class HelpDonation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_donation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipient_id', 'transfer_date'], 'integer'],
            [['amount', 'transfer_date', 'payment_system','p_date'], 'required'],
            [['amount'], 'number'],
            [['payment_system', 'comment'], 'string'],
            [['p_date'], 'date']
        ];
    }

    public function getP_date() {
        return $this->transfer_date ? Yii::$app->formatter->asDate($this->transfer_date) : '';
    }

    public function setP_date($date) {
        $this->transfer_date = $date ? strtotime($date) : null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор записи',
            'recipient_id' => 'Получатель',
            'amount' => 'Сумма пожертвования',
            'transfer_date' => 'Время перевода',
            'p_date' => 'Время перевода',
            'payment_system' => 'Система оплаты',
            'comment' => 'Пометки'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(HelpRecipient::className(), ['id' => 'recipient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }
}
