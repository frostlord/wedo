<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "help_recipient".
 *
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $amount
 * @property integer $is_active
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 *
 * @property Attachments[] $attachments
 * @property HelpDonation[] $helpDonations
 * @property User $createdBy
 * @property User $updateBy
 */
class HelpRecipient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_recipient';
    }

    public function fields()
    {
        return
        [
            'id' =>'id',
            'title' => 'name',
            //'thumb'	=> '', //URL	URL-картинки для предварительного просмотра. Базовый размер 145x145.
            'images' => function(){return (count($this->attachments)>0)? ArrayHelper::getColumn($this->attachments, 'path') : null;},
            'description' => 'description',
            'donation_id'=>'id',
            'donations_collected' => 'receivedamount',
            'donations_needed' => 'amount',
        ];
    }
    /*
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'amount'], 'required'],
            [['description'], 'string'],
            [['amount'], 'number'],
            [['is_active', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           // 'id' => 'Идентификатор записи',
            'name' => 'Наименование (получателя/проекта)',
            'description' => 'Описание',
            'amount' => 'Необходимая сумма (kzt)',
            'receivedamount' => 'Полученно (kzt)',
            'is_active' => 'Активность'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachments::className(), ['recipient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHelpDonations()
    {
        return $this->hasMany(HelpDonation::className(), ['recipient_id' => 'id']);
    }

    public static function get_recipient()
    {
        return ArrayHelper::map(HelpRecipient::find()->all(),'id','name');
    }
    public function getReceivedamount()
    {

        $rates = $this->helpDonations;
        $count = count($rates);
        if ($count!=0)
        {
            $sum = 0;
            foreach($rates as $i)
            {
                $sum = $sum + $i->amount;
            }
            return $sum;
        }
        return 0;
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }
}
